#include <iostream>

using namespace std;

const int MAXV = 19;

unsigned int adjA[MAXV];
unsigned int adjB[MAXV];
unsigned int NNA[MAXV];
unsigned int NNB[MAXV];
unsigned int ES[MAXV];
int degA[MAXV];
int degB[MAXV];
int edges = 0;

bool connected(int A, int B) {
    unsigned int SA = 1;
    unsigned int SB = 0;
    unsigned int S = 1;

    for (int i = 0; S != 0; ++i) {
        unsigned int neigh = 0;

        while (S != 0) {
            int v = __builtin_ffs(S) - 1;
            S ^= (1u << v);

            if (i & 1) {
                neigh |= adjB[v];
            } else {
                neigh |= adjA[v];
            }
        }

        if (i & 1) {
            S = (~SA & neigh);
            SA |= S;
        } else {
            S = (~SB & neigh);
            SB |= S;
        }
    }

    return __builtin_popcount(SA) == A && __builtin_popcount(SB) == B;
}

bool validate(int maxB, int A, int B, int c) {
    if (degA[0] <= 3 && degA[A - c] <= 3 && maxB <= 3) {
        return true; // sub cube
    }

    int minB = *min_element(degB, degB + B);

    if (minB == 0) {
        return true; // isolated
    }

    if (c < A && degA[A - c - 1] == 1 && minB == 1) {
        return true; // lemma
    }

    if (!connected(A, B)) {
        return true; // not connected
    }

    bool flagA = (c < A && degA[A - c - 1] == 1);
    bool flagB = (minB == 1);

    for (int v = 0; v < A; ++v) {
        auto E = adjA[v];

        while (E != 0) {
            int u = __builtin_ffs(E) - 1;
            E ^= (1u << u);
            NNA[v] |= adjB[u];
            NNB[u] |= adjA[v];
        }
    }

    for (int v = 0; v < A && (!flagA || !flagB); ++v) {
        auto E = adjA[v];

        while (E != 0 && (!flagA || !flagB)) {
            int u = __builtin_ffs(E) - 1;
            E ^= (1u << u);

            if ((NNA[v] & adjB[u]) == adjB[u]) {
                flagB = true;
            }

            if ((NNB[u] & adjA[v]) == adjA[v]) {
                flagA = true;
            }
        }
    }

    for (int v = 0; v < A && !flagB; ++v) {
        auto E = adjA[v];
        int nn = 0;

        while (E != 0 && !flagB) {
            int u = __builtin_ffs(E) - 1;
            E ^= (1u << u);
            ES[nn++] = u;

            for (int j = 0; j < nn - 1; ++j) {
                if ((NNA[v] & (adjB[ES[j]] | adjB[ES[nn - 1]])) == (adjB[ES[j]] | adjB[ES[nn - 1]])) {
                    flagB = true;
                    break;
                }
            }
        }
    }

    for (int v = 0; v < B && !flagA; ++v) {
        auto E = adjB[v];
        int nn = 0;

        while (E != 0 && !flagA) {
            int u = __builtin_ffs(E) - 1;
            E ^= (1u << u);
            ES[nn++] = u;

            for (int j = 0; j < nn - 1; ++j) {
                if ((NNB[v] & (adjA[ES[j]] | adjA[ES[nn - 1]])) == (adjA[ES[j]] | adjA[ES[nn - 1]])) {
                    flagA = true;
                    break;
                }
            }
        }
    }

    return flagA && flagB;
}

bool phase23(int v, unsigned int interval_mask, int l, int maxB, int A, int B, int c) {
    while (v < A && l >= B) {
        ++v;
        l = (v >= A - c) ? c : 0;
    }

    if (v >= A) {
        return validate(maxB, A, B, c);
    }

    int r = l + 1;

    while (r < B && ((interval_mask >> r) & 1) == 0) {
        ++r;
    }

    int u = l;
    int prev;

    if (v == A - c) {
        prev = B - c + 2;
    } else if (v == 0) {
        prev = B;
    } else {
        prev = degA[v - 1];
    }

    if (v >= A - c && edges - degA[v] + (A - v) * (prev - 2) < A + B)
        return true;

    if (v < A - c && edges - degA[v] + (A - c - v) * prev + c * (B - c) < A + B)
        return true;

    if (degA[v] > 0 && !phase23(v, interval_mask, r, maxB, A, B, c))
        return false;

    for (; u < r && degA[v] + 1 <= prev; ++u) {
        adjA[v] |= (1u << u);
        adjB[u] |= (1u << v);
        ++degA[v];
        maxB = max(maxB, ++degB[u]);
        ++edges;

        if (!phase23(v, (interval_mask | (1u << (u + 1))), r, maxB, A, B, c))
            return false;
    }

    for (--u; u >= l; --u) {
        adjA[v] ^= (1u << u);
        adjB[u] ^= (1u << v);
        --degA[v];
        --degB[u];
        --edges;
    }

    return true;
}

bool phase1(int A, int B, int C) {
    int c = C / 2;
    edges = 0;

    for (int v = 0; v < c; ++v) {
        adjA[A - c + v] |= (1u << v);
        adjB[v] |= (1u << (A - c + v));
        adjA[A - c + (v + 1) % c] |= (1u << v);
        adjB[v] |= (1u << (A - c + (v + 1) % c));
        ++degA[A - c + v];
        ++degA[A - c + (v + 1) % c];
        degB[v] += 2;
        edges += 2;
    }

    return phase23(0, (1u | (1u << c) | (1u << B)), c, 0, A, B, c);
}

bool check(int A, int B, int C) {
    fill(adjA, adjA + A, 0u);
    fill(adjB, adjB + B, 0u);
    fill(degA, degA + A, 0u);
    fill(degB, degB + B, 0u);
    return phase1(A, B, C);
}

int main() {
    for (int V = 6; V < MAXV; ++V) {
        printf("Results for V = %d:\n", V);

        for (int A = 3; 2 * A <= V; ++A) {
            int B = V - A;

            for (int C = 6; C / 2 <= A; C += 2) {
                bool result = check(A, B, C);

                if (!result) {
                    printf("Something goes wrong\n");
                    exit(0);
                }

                printf("A = %d, B = %d, C = %d, ok\n", A, B, C);
            }
        }
    }

    return 0;
}
